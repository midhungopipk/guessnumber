import React, {useState}from "react";
import {View, StyleSheet, Text, TextInput, Button, TouchableWithoutFeedback, Keyboard, Alert} from 'react-native';
import Card from '../components/Card';
import Input from "../components/Input";
import Colors from "../constants/Colors";
import NumberContainer from "../components/NumberContainer";
import MainButton from "../components/MainButton";




const StartGameScreen=props=>{
    const [enteredValue,setEnteredValue] = useState('');
    const [confirmed,setConfirmed]=useState(false);
    const [selectedNumber,setSelectedNumber]=useState()
    

    const inputHandler = inputNumber=>{
        setEnteredValue(inputNumber.replace(/[^0-9]/g,''));
    }

    const resetInputHandler=()=>{
        setEnteredValue('')
        setConfirmed(false) 
    }

    const confirmInputHandler = () =>{
        const choosenNumber = parseInt(enteredValue);

        if(isNaN(choosenNumber) || choosenNumber<= 0 || choosenNumber > 99 ){
            Alert.alert('Invalid Number!!!',
            'Only Numbers between 1-99 Only',
            [{text:'Okay',style:"destructive",onPress: resetInputHandler}])
            return ;
        }
        setConfirmed(true);
        setSelectedNumber(choosenNumber);
        setEnteredValue('');
        Keyboard.dismiss();
    }

    let confirmedOutput;

    if(confirmed){
        confirmedOutput = <Card style={styles.choosenNumberCard}>
                             
                              <Text>Confirm Your Selection:</Text>
                              <NumberContainer>{selectedNumber}</NumberContainer>
                              <MainButton onPress={()=>{props.onStartGame(selectedNumber)}}>START GAME</MainButton>
                            
                           </Card>
    
                        
    }

    return(
       <TouchableWithoutFeedback onPress={()=>{
           Keyboard.dismiss()
       }}>
            <View style={styles.screen}>
            <Text style={styles.title}>Start a New Game</Text>
            <Card style={styles.inputContainer}>
                
                <Text>Enter a Number</Text>

                <Input style={styles.input} keyboardType="number-pad" maxLength={2} onChangeText={inputHandler} value={enteredValue}/>

                <View style={styles.buttonContainer}>
                    <View style={styles.button}><Button color={Colors.primary} title="Reset" onPress={resetInputHandler}/></View>
                    <View style={styles.button}><Button color={Colors.accent} title="Confirm" onPress={confirmInputHandler}/></View>
                </View>

                
            </Card>

            
            {confirmedOutput}
                
            

        </View>
       </TouchableWithoutFeedback>
    );
};

const styles=StyleSheet.create({
    screen:{
        flex:1,
        padding:10,
        alignItems:"center"

    },
    inputContainer:{
        width:300,
        maxWidth:"80%",
        alignItems:"center"
    

    },
    buttonContainer:{
        flexDirection:"row",
        width:"100%",
        justifyContent:"space-between",
        paddingHorizontal:15
        
        
    },
    
    title:{
        fontSize:20,
        margin:20

    },
    
    button:{
        width:80
        
    },
    input:{
        textAlign:"center"
    },
    choosenNumberCard:{
        alignItems:"center",
        justifyContent:"center",
        marginTop:50,
        borderRadius:9
        
    }

});

export default StartGameScreen;