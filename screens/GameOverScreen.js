import React from "react";
import {View, Text, StyleSheet, Button, Image} from 'react-native';
import Card from "../components/Card";
import MainButton from "../components/MainButton";
import Colors from "../constants/Colors";

const GameOverScreen = props =>{
    return(
        
            <View style={styles.gameOverCard}>
                <Card style={styles.resultTextCard}>
                    <View style={styles.gameover}>
                        <Text style={styles.winText} >GameOver!!! your number is</Text>
                        <Text style={styles.guessedNumber}> {props.yourNumber}</Text> 
                        <Text style={styles.winText}>Am I right??</Text>
                        
                        <Image source={require('../assets/success.png')} style={styles.image} resizeMode="cover"/>
                        
                        <Text style={styles.winText}>I Found it in <Text style={styles.guessedNumber}>{props.numOfRounds}</Text> rounds.</Text>
                        
                    </View>
                    <View style={styles.button}>

                        <MainButton onPress={props.onRestart}>RESTART</MainButton>

                    </View>
                </Card>
            </View>
         
        
        
    );
}

const styles = StyleSheet.create({
    gameOverCard:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
                
    },
    image:{
        width:300,
        height:300,
        margin:20,
        borderRadius:1000,
        borderColor:"black",
        borderWidth:2

        
    },
    gameover:{
        alignItems:"center",
        justifyContent:"center",
        marginVertical:20
        
    },
    
    guessedNumber:{
        color: Colors.primary,
        fontSize:30
    },
    resultTextCard:{
        width:"80%"
        
    },
    button:{
        alignItems:"center"
    
    },
    winText:{
        color:Colors.accent,
        fontSize:20
    }

})

export default GameOverScreen;