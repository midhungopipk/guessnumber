import React, {useState, useRef, useEffect} from "react";
import { View, Text, StyleSheet, Button, Alert, ScrollView, FlatList } from "react-native";
import Card from "../components/Card";
import NumberContainer from "../components/NumberContainer";
// import Colors from "../constants/Colors";
import MainButton from "../components/MainButton";
import {Ionicons} from '@expo/vector-icons';


const generateRandomBetween=(min,max,exclude)=>{
    min=Math.ceil(min);
    max=Math.floor(max);
    const rndNum= Math.floor(Math.random()*(max-min))+min;
    if(rndNum===exclude){
        return generateRandomBetween(min, max, exclude);
    }else{
        return rndNum;
    }
}

// const renderView = (value,numOfRound)=>{
//     return(
//             <View key={value} style={styles.listItems}>
//                 <Text>#{numOfRound}</Text>
//                 <Text>{value}</Text>
//             </View>
                                                 
//     );
// }

const renderView = (listLength,itemData)=>{
    return(
            <View style={styles.listItems}>
                <Text>#{listLength-itemData.index}</Text>
                <Text>{itemData.item}</Text>
            </View>
                                                 
    );
}



const GameScreen = props =>{
    const initialGuess= generateRandomBetween(1 , 100, props.userChoice);
    const [currentGuess, setCurrentGuess]=useState(initialGuess);
    const [pastGuess,setPastGuess]=useState([initialGuess])

    const currentLow = useRef(1);
    const currentHigh = useRef(100);

    const {userChoice, onGameOver} = props; 
    //now we can remove props. from the below functions.
    useEffect(()=>{
        if(currentGuess==userChoice){
            onGameOver(pastGuess.length);

        }
    },[currentGuess, userChoice, onGameOver]);
    // ^these are dependencies. this function only rerun when these values changed. 

    const nextGuessHandler = direction =>{

        if(
            (direction==="lower" && currentGuess < props.userChoice)||(direction==="greater" && currentGuess > props.userChoice)
            ){
            Alert.alert('Don\'t lie', 'Don\'t cheat!! wrong hint',[{text:'sorry!!' , style:'cancel'}])
            return;
        }

        if(direction==="lower"){
            currentHigh.current = currentGuess
        }else{
            currentLow.current = currentGuess+1;      //this +1 is to make current low increment by 1 bcoz if we get 3 as a guess and we confirm a number greater than 3 we don't wanto print 3 again
                                                      // so we make currentlow as 4(included) and we are excluding the currenthigh in logic
        }
        const nextNumber = generateRandomBetween(currentLow.current , currentHigh.current , currentGuess);
        setCurrentGuess(nextNumber);

        setPastGuess(curGuess=>[nextNumber,...curGuess]);//adding every guesses into pastGuess list


    }
    
    return(
        <View style={styles.screen}>
            <Text>computers Guess:</Text>
            <NumberContainer>{currentGuess}</NumberContainer>
            <Card style={styles.buttonContainer}>
                <MainButton onPress={nextGuessHandler.bind(this,"lower")}><Ionicons name="md-remove" size={25}/> </MainButton> 
                <MainButton onPress={nextGuessHandler.bind(this,"greater")}><Ionicons name="md-add" size={25}/></MainButton>
            </Card>
            <View style={styles.listContainer}>
                {/* <ScrollView contentContainerStyle={styles.list}>
                    {pastGuess.map((guess,index)=>renderView(guess,pastGuess.length-index))}
                </ScrollView> */}
                <FlatList
                keyExtractor={item=>item}
                data={pastGuess}
                renderItem={renderView.bind(this,pastGuess.length)}
                contentContainerStyle={styles.list}
                />
            </View>
        </View>
    );
    //insted of LOWER and GREATER we can use <IonIcons/>

}



const styles = StyleSheet.create({
    screen:{
        flex:1,
        padding:20,
        alignItems:"center"
    },

    buttonContainer:{
        flexDirection:"row",
        justifyContent:"space-around",
        marginTop:10,
        width:300
    },
    list:{
        alignItems:"center"
    },
    listContainer:{
        width:"80%",
        flex:1
    },
    listItems:{
        borderColor:"black",
        borderWidth:2,
        padding:15,
        backgroundColor:"white",
        marginVertical:30,
        elevation:9,
        borderRadius:10,
        alignItems:"center",
        flexDirection:"row",
        justifyContent:"space-between",
        width:"50%"
    }

})

export default GameScreen;