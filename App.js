import React, {useState} from 'react';
import { StyleSheet, View,SafeAreaView } from 'react-native';
// import * as Font from 'expo-font';
// import AppLoading from 'expo-app-loading';

import Header from './components/Header';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import GameOverScreen from './screens/GameOverScreen';


// const fetchFonts = () =>{
//   return Font.loadAsync({ //loadAsync isa a function on font package which we are importing here allows you to load fonts
//                           //and what you pass inside  loadAsync is an object where you tell expo and ReactNative about all fonts i wanna load
//     'open-sans-regular':require('./assets/fonts/OpenSans-Regular'),
//     'open-sans-bold':require('./assets/fonts/OpenSans-Bold')
//   })
// }

// this function will not load instantaneously 
//so returning this promise  inside fetchFonts will help to wait for the promise to resolve and load the fonts.

export default function App() {
  const [userNumber,setUserNumber]=useState();
  const [guessRounds, setGuessRounds] = useState(0)

  // const [dataLoaded, setDataLoded] = useState(false);

  // if (!dataLoaded){
  //   return <AppLoading 
  //                     startAsync={fetchFonts} 
  //                     onFinish={()=> setDataLoded(true)} 
  //                     onError={(err)=>(console.log(err))}/> 
   // }

    //startAsync is not an inbuilt prop it's just a prop to point fetchFont function.
    // after that onFinish will setDataLoaded to true so that all other screen appear.
 

  const newGameHandler = () =>{
    setGuessRounds(0); //when new game starts we need to set guessrounds to 0
    setUserNumber(null); //Also we need to set usernumber to a false value to restart the game
  }

  const gameOverHandler = numOfRounds =>{
    setGuessRounds(numOfRounds)
  }

  const startGameHandler = selectedNumber=>{
    setUserNumber(selectedNumber);
    
  }

  let content = <StartGameScreen onStartGame={startGameHandler}/>;

 

  if(userNumber && guessRounds <=0){
    content = <GameScreen userChoice={userNumber} onGameOver={gameOverHandler}/>;
    
  }else if (guessRounds>0){
    content=<GameOverScreen yourNumber={userNumber} numOfRounds={guessRounds} onRestart={newGameHandler}/>;
  }
  return (
    <SafeAreaView style={styles.screen}>  
        <Header title="Guess The Number"/>
        {content}
    </SafeAreaView>
  );
}   //SafeArea View Minds the Notches in the displa screen that most modern phones have.

const styles = StyleSheet.create({
  screen:{
    flex:1
  }
  
});
