import React from "react";
import {View, Text, TouchableOpacity, StyleSheet,TouchableNativeFeedback,Platform} from 'react-native';
import Colors from "../constants/Colors";


const MainButton = props =>{

    let ButtonComponent = TouchableOpacity;
    
    if(Platform.OS=='android' && Platform.Version >= 21){     // Platform checks whether the os is android or ios and set ripple effect that is (TouchableNativeFeedback.)
        ButtonComponent = TouchableNativeFeedback;
    }
    return(
        <View style={styles.buttonContainer}>
            <ButtonComponent activeOpacity={0.6} onPress={props.onPress}>
                <View style={styles.button}>
                    <Text style={styles.buttonText}> {props.children}</Text>
                </View>
            </ButtonComponent>
        </View>
    );

}

const styles = StyleSheet.create({
    buttonContainer:{
        borderRadius:25,
        overflow:"hidden",          //just cut off the overflowing things out of this view
        elevation:79
    },
    button:{
        backgroundColor: Colors.accent,
        padding:15,
        borderRadius:25


    },
    buttonText:{
        color:"white",
        fontSize:18,
        fontWeight:"bold"

    }

})

export default MainButton;