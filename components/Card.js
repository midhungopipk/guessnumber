import React from "react";
import {View, Text, StyleSheet} from 'react-native';

const Card = props=>{
    return(
        <View style={{...styles.card,...props.style}}>{props.children}</View>
    )
};

const styles=StyleSheet.create({
    card:{
        paddingTop:30,
        backgroundColor:"white",
        shadowColor:"black",
        shadowOffset:{width:0, height:3},
        shadowOpacity:1,
        shadowRadius:6,
        elevation:50,
        borderRadius:30,
        padding:20
        
    }

})


export default Card;